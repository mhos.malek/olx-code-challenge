module.exports = {
  runtimeCompiler: true,
  css: {
    loaderOptions: {
      scss: {
        prependData: `
                        @import "@/styles/_variables.scss";
                        @import "@/styles/index.scss";
                        // default responsive sizes
                        $font-size--mobile-large: 14px;
                        $device-size--desktop-large: 1200px;
                        $device-size--desktop-laptop: 1024px;
                        $device-size--tablet: 768px;
                        $device-size--mobile-large: 430px;
                        $device-size--mobile-medium: 360px;
                        $device-size--mobile-small: 320px;
                    `
      }
    }
  }
};
