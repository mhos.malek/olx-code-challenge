# olx-code

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Vue Config  
as this [github issue](https://github.com/vuejs/vue-loader/issues/501) suggests, it's not possible to compose sass modules in vue components. so I have to used some other ways to handle this. [this](https://github.com/vuejs/vue-loader/issues/328) is the description of soultion that I have used.


### some words before you see this code challenge:
 I have a  React background. so clearly I feel I have developed something in the project in a React way. So I hope it just working well. it was a good challenge for me! thanks from you for that!

- I have read offcial Style guides, so many of naming convetions (especially components name and store names) based on VUE Style guide.
- this is no real form or submit action on project. I have just used very simple validation for just test some value.
- this is just an MVP and I  developed it as fast as I can. so this is not the best solution for some problems and I know that.
- I have created a customized font library (olx custom font library) and used it for font icons. it's replaced with fontawesome.
- the backend is just going to response a dummy json and it doesn't have any real action.

there is no more thing to talk about! hope you find it well.

