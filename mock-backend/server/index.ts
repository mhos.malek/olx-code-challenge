import { delay } from "../utils";
// @ts-ignore
import sideMenuData from "../data/sideMenuData.json";
// @ts-ignore
import requestsData from "../data/requests.json";
// @ts-ignore
import item_sample_data from "../data/item_sample_data.json";
// @ts-ignore
import { SERVER_DELAY_TIME } from "../config";

class Backend {
  SERVER_DELAY_TIME: number;
  constructor() {
    this.SERVER_DELAY_TIME = SERVER_DELAY_TIME;
  }
  getDataTypeInformationById(showError = false, id: number) {
    return new Promise((resolve, reject) => {
      if (showError) {
        reject("Error in fetch Data");
      }
      delay(this.SERVER_DELAY_TIME).then(() => {
        // simulate server response
        const { data } = item_sample_data;
        const response = data.filter(
          (item: any, index: number) => index === id
        );
        resolve(response[0]);
      });
    });
  }

  getDataTypesList(showError = false) {
    return new Promise((resolve, reject) => {
      if (showError) {
        reject("Error in fetch Data");
      }
      delay(this.SERVER_DELAY_TIME).then(() => resolve(sideMenuData)); // simulate server response
    });
  }

  getRequestsList(showError = false) {
    return new Promise((resolve, reject) => {
      if (showError) {
        reject("Error in fetch Data");
      }
      delay(this.SERVER_DELAY_TIME).then(() => resolve(requestsData)); // simulate server response
    });
  }
}

export default new Backend();
