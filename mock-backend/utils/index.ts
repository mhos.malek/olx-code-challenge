const delay = (timeInMs: number) => {
    return new Promise((resolve) => setTimeout(() => resolve(), timeInMs));
};

export {
    delay
}