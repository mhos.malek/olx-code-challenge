import state from "./requestState";
import mutations from "./requestMutations";
import getters from "./requestGetters";
import actions from "./requestActions";

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
};
