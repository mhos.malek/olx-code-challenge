import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import ActionTypes from "./action_types";
import * as ApiRequests from "../services/APIRequest";
import * as utils from "@/utils/general";
interface RequestType {
  status: String;
  date: String;
  reason: String;
}

export default new Vuex.Store({
  state: {
    requestsListAll: [],
    requestsListPending: [],
    requestsListApproved: [],
    requestsListDenied: [],
    dataTypesList: [],
    selectedDataType: {},
    showLoadingDataType: false,
    showLoadingVerticalMenu: false,
    showLoadingRequestsList: false
  },
  mutations: {
    [ActionTypes.REQUESTS_LIST_REQUESTED](state) {
      state.showLoadingRequestsList = true;
    },

    [ActionTypes.REQUESTS_LIST_FULFILLED](state, payload) {
      state.showLoadingRequestsList = false;

      state.requestsListAll = payload.data;
    },
    [ActionTypes.REQUESTS_LIST_FAILD](state) {
      state.showLoadingRequestsList = false;
    },

    [ActionTypes.DATA_TYPES_REQUESTED](state) {
      state.showLoadingVerticalMenu = true;
    },

    [ActionTypes.DATA_TYPES_FULFILLED](state, payload) {
      state.dataTypesList = payload.data;
      state.showLoadingVerticalMenu = false;
    },
    [ActionTypes.DATA_TYPES_FAILD](state) {
      state.showLoadingVerticalMenu = false;
    },

    [ActionTypes.DATA_TYPES_BY_ID_REQUESTED](state) {
      state.showLoadingDataType = true;
    },

    [ActionTypes.DATA_TYPES_BY_ID_FULFILLED](state, payload) {
      state.showLoadingDataType = false;
      state.selectedDataType = payload.data;
    },
    [ActionTypes.DATA_TYPES_BY_ID_FAILD](state) {
      state.showLoadingDataType = false;
    },
    [ActionTypes.ADD_NEW_POSSIBLE_VALUE](state, payload) {
      /// @ts-ignore
      state.selectedDataType.possibleValues = [
        // @ts-ignore
        ...state.selectedDataType.possibleValues,
        payload
      ];
    }
  },
  getters: {
    requestsListPending: state => {
      return state.requestsListAll.filter(
        (request: RequestType) => request.status === "Pending"
      );
    },
    requestsListApproved: state => {
      return state.requestsListAll.filter(
        (request: RequestType) => request.status === "Approved"
      );
    },
    requestsListDenied: state => {
      return state.requestsListAll.filter(
        (request: RequestType) => request.status === "Denied"
      );
    }
  },
  actions: {
    async getRequestsFromApi({ commit }) {
      commit(ActionTypes.REQUESTS_LIST_REQUESTED);
      try {
        const response: any = await ApiRequests.getRequestsList();

        const convertedDate = response.data.reduce(
          (initilizeValue: any, currentItem: RequestType, index: number) => {
            return (currentItem.date = utils.convertDateToString(
              currentItem.date
            ));
          },
          []
        );
        commit(ActionTypes.REQUESTS_LIST_FULFILLED, { data: response.data });
      } catch (error) {
        commit(ActionTypes.REQUESTS_LIST_FAILD);
      }
    },
    async getDataTypeListFromApi({ commit }) {
      commit(ActionTypes.DATA_TYPES_REQUESTED);
      try {
        const response: any = await ApiRequests.getDataTypesList();
        commit(ActionTypes.DATA_TYPES_FULFILLED, { data: response.data });
      } catch (error) {
        commit(ActionTypes.DATA_TYPES_FAILD);
      }
    },
    async getDataTypeInformationFromApi({ commit }, payload) {
      commit(ActionTypes.DATA_TYPES_BY_ID_REQUESTED);
      try {
        const { itemId } = payload;
        const response: any = await ApiRequests.getDataTypeInformationById(
          itemId
        );
        commit(ActionTypes.DATA_TYPES_BY_ID_FULFILLED, { data: response });
      } catch (error) {
        commit(ActionTypes.DATA_TYPES_BY_ID_FAILD);
      }
    }
  },
  modules: {}
});
