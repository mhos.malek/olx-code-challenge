// plugins/modules

import Vue from "vue";
import VueRouter from "vue-router";

// init data we need
import App from "./App.vue";
import store from "./store";
import router from "./router";

// filters
import "./vuefilters/UpperCase";

Vue.config.productionTip = false;
Vue.use(VueRouter);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
