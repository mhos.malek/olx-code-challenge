export default {
  general: {
    title: "Data Gate",
    slogan: "some slogan"
  },
  defaultSizes: {
    desktopLarge: 1200,
    desktopLaptop: 1024,
    tablet: 768,
    mobileLarge: 430,
    mobileMedium: 360,
    mobileSmall: 320
  }
};
