import Backend from "../../mock-backend/server";

const getRequestsList = () => {
  return Backend.getRequestsList();
};

const getDataTypesList = () => {
  return Backend.getDataTypesList();
};

const getDataTypeInformationById = (itemId: number) => {
  return Backend.getDataTypeInformationById(false, itemId);
};
export { getRequestsList, getDataTypesList, getDataTypeInformationById };
