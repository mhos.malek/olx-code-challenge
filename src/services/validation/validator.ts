const validator = (value: string, rule: any) => {
  if ((rule.name === "minLength", rule.value)) {
    if (value.length < rule.value) {
      return false;
    }
    return true;
  }
  if ((rule.name === "maxLength", rule.value)) {
    if (value.length > rule.value) {
      return false;
    }
    return true;
  }
};

export default validator;
