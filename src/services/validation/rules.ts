const minLength = {
  length: 2
};

const maxLength = {
  length: 4
};

export { minLength, maxLength };
