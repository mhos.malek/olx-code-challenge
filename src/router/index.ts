import VueRouter from "vue-router";
import { routes } from "./routes";

const router = new VueRouter({
  linkActiveClass: "active-menu-link",
  mode: "history",
  routes
});

export default router;
