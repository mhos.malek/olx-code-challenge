// @ts-ignore
import ManageDataPage from "@/components/pages/ManageData/ManageDataPage";
// @ts-ignore
import RequestsPage from "@/components/pages/Requests/RequestsPage";
// @ts-ignore
import NotFoundPage from "@/components/pages/NotFound/NotFoundPage";
export const routes = [
  {
    path: "/",
    redirect: "/manage"
  },
  {
    path: "/manage",
    component: ManageDataPage,
    name: "ManageData",
    meta: {
      title: "Managa Data",
      metaTags: [
        {
          name: "description",
          content: "The Manage Data Page for project."
        },
        {
          property: "og:description",
          content: "The Manage Data Page for project."
        }
      ]
    }
  },
  {
    path: "/requests",
    component: RequestsPage,
    name: "Requests",
    meta: {
      title: "Requests",
      metaTags: [
        {
          name: "description",
          content: "The Request Page for project."
        },
        {
          property: "og:description",
          content: "The Request Page for project."
        }
      ]
    }
  },
  {
    path: "/review",
    component: NotFoundPage,
    name: "Review",
    meta: { title: "Review" }
  },
  {
    path: "*",
    component: NotFoundPage,
    name: "NotFound",
    meta: { title: "Not Found " }
  }
];
