// @ts-ignore
import TheMainLayout from "./TheMainLayout/TheMainLayout";
// @ts-ignore
import TheHeader from "./TheHeader/TheHeader";
// @ts-ignore
import TheFooter from "./TheFooter/TheFooter";
// @ts-ignore
import ThePageTitle from "./ThePageTitle/ThePageTitle";
// @ts-ignore
import TheActionBar from "./TheActionBar/TheActionBar";

export { TheMainLayout, TheHeader, TheFooter, ThePageTitle, TheActionBar };
