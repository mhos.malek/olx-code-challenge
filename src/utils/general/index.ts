const convertDateToString = (date: any) => {
  return date.slice(0, 10).replace(/-/g, "");
};

const getContaierWidth = () => {
  const windowWidth = window.innerWidth;
  const defaultSizes = {
    desktopLarge: 1200,
    desktopLaptop: 1024,
    tablet: 768,
    mobileLarge: 430,
    mobileMedium: 360,
    mobileSmall: 320
  };
  if (windowWidth >= defaultSizes.desktopLarge) {
    return 1200;
  }
  if (windowWidth >= defaultSizes.desktopLaptop) {
    return 980;
  }
  if (windowWidth >= defaultSizes.tablet) {
    return 720;
  }
  if (windowWidth > defaultSizes.mobileLarge) {
    return 430;
  }
  if (windowWidth > defaultSizes.mobileMedium) {
    return 320;
  }
  if (windowWidth > defaultSizes.mobileSmall) {
    return 310;
  }
};

export { getContaierWidth, convertDateToString };
