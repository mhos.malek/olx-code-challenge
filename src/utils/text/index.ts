const upperCaseString = (string: string) => {
  return string.toUpperCase();
};

export { upperCaseString };
