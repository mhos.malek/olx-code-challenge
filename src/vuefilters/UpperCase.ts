import Vue from "vue";

Vue.filter("upperCase", function(value: string) {
  return value.toUpperCase();
});
